CC = gcc
LD = gcc

tool: tool.c psx.o
	$(LD) -g -o $@ $< psx.o -ggdb3
psx.o: psx.c
	$(CC) -g -o $@ -c $< -msse4.2 -ggdb3
tool_release:
	$(CC) $(CFLAGS) -o psx.o -c psx.c -msse4.2
	$(CC) $(CFLAGS) -o tool.o -c tool.c
	$(LD) $(LDFLAGS) -o psxtool -O2 tool.o psx.o
libs:
	$(CC) $(SHLIB_CFLAGS) -fPIC -o psx.o -c psx.c -msse4.2
	$(LD) $(SHLIB_LDFLAGS) -shared -o libpsx.so psx.o
	$(CC) $(STATICLIB_CFLAGS) -fPIC -o psx.o -c psx.c -msse4.2
	ar rcs libpsx.a psx.o
docs:
	mkdir -p docs
	doxygen
clean:
	rm -rf docs psx.o tool psxtool test.psx libpsx.so libpsx.a