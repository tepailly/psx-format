# PSX file format

## Introduction
This repository contains the implementation of the RADAR PSX binary file format.
The PSX file format was made in an effort to be simple, fast and flexible (in this order).

## Compatibility

* The PSX file format will **not fragment** the file into smaller ones, make sure to use a mordern filesystem (not FAT32).
* A PSX file contains a CRC32 hash that is computed using SSE4.2 instructions : this C implementation requires a CPU that supports SSE4.2

## Implementations

As of now, PSX file format is implemented in 3 languages.

| Language | Read | Metadata | Write |
|----------|------|----------|-------|
| C        | X    | X        | X     |
| Matlab   | X    |          |       |
| Python   | X    |          |       |

## Documentation
The documentation for this format can be found in the ```PSX_File_Format_Specification.pdf``` document.

## Getting started
To get started with the PSX file format, you can first clone this repository with the command below.

```git clone https://gitlab.tudelft.nl/tepailly/psx-format.git```

Then, you can generate the C API docs with ```make docs```, then browsing to ```docs/html/index.html```.

You can also look in the ```test``` folder where example programs are provied.

## Building

No particular dependencies are required, you should have ```make``` installed and your CPU should support SSE4.2 for the C implementation.

To build the ```psx_tool```, run :
```make tool_release```

To build the libraries, run :
```make libs```

## Testing

To test the correct implementation of the PSX file format, a simple but wide test suite is provided in ```tests/c```.

The code contained in ```test_psx.c``` will test all of the features of the file format. This test code will make sure that
both the data and user metadata written in the file is the same when read back. It will also check the CRC.

## Tool usage

This simple tool will read back the metadata of the PSX file and check it's CRC.

Here is an example run.
```
$ ./psxtool test.psx
---- PSXTool ----
Metadata of PSX file "test.psx"
 - PSX file version 1.3
 - Number of frames : 4
 - Number of channels per frame : 2
 - Number of samples per channel : 16
 - Size of a single sample : 8 bytes
 - Size of the user metadata : 16 bytes
 - CRC match : 0x8B8BA9A2
```

## Continuous integration

In the 2 stage CI, a first build is run on an alpine image, the build is then tested for C and Python.

If the above succeds, a Holy Build Box (HBB) is used to produce a shared library, a static library and 
the ```psxtool``` executable.