/**
 * @file test_psx.c
 * @author Thomas Epailly (thomas.epailly@alumni.enac.fr)
 * @brief Test code for the PSX file format.
 * @details Writes data into a PSX file, then reads and checks the data read back from it.
 * @version 0.1
 * @date 2024-07-05
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#define CHANNEL_SIZE 16
#define TYPE_SIZE sizeof(uint64_t)
#define NB_FRAMES 4
#define NB_CHANNELS 2
#define FN "../../test.psx"

#include "psx.h"
#include "parsax_metadata.h"

int main() {
    int err = 0;
    // Initialize PSX
    PSXFile file;
    PARSAX_Metadata met = PARSAX_METADATA_INIT;
    met.window = PARSAX_WIN_RECT;
    met.domain = PARSAX_DOMAIN_FREQUENCY;
    met.mixer_disabled = 1;
    met.gain_index = 2;
    met.sample_delay = 3;
    met.window_offset = 4;
    met.timestamp_s = (uint64_t) time(NULL);

    // #### Write into file first ####
    err = PSX_open(&file, FN, PSX_MODE_WRITE);
    if(err > 0) {
        fprintf(stderr, "[ERROR] Could not open output file.\n");
        return 1;
    }

    printf("[INFO] PSX file opened for write\n");

    PSX_set_metadata_size(&file, sizeof(PARSAX_Metadata));
    PSX_set_type_size(&file, TYPE_SIZE);
    PSX_set_nb_channels(&file, NB_CHANNELS);
    PSX_set_channel_size(&file, CHANNEL_SIZE);

    int64_t *samples = calloc(CHANNEL_SIZE, TYPE_SIZE);
    size_t datasize = TYPE_SIZE * CHANNEL_SIZE; 

    for (int frame=0; frame<NB_FRAMES; frame++) {
        for (int chan=0; chan<NB_CHANNELS; chan++) {
            //memset(samples, chan+1, 4*NB_SAMPLES);
            samples[0] = frame;
            samples[1] = chan;
            for (int i=2; i<CHANNEL_SIZE-1; ++i) {
                samples[i] = i + frame + chan; // Write different data in each frame and channel
            }
            samples[CHANNEL_SIZE-1] = 0xff;
            PSX_write_channel(&file, samples);
        }
    }
    PSX_set_metadata(&file, &met);
    printf("[INFO] Closing file...");
    PSX_add_CRC(&file);
    err = PSX_close(&file);
    printf("OK\n");
    assert(err == 0);
    printf("[INFO] Test file written\n");
    free(samples);

    // #### Read back from the file ####
    err = PSX_open(&file, FN, PSX_MODE_READ);
    assert(err == 0);
    printf("[INFO] Test file opened for read\n");
    if(PSX_check_CRC(&file) == 0)
        printf("[INFO] CRC match\n");
    else
        printf("[WARNING] CRC mismath\n");
    // Check if metadata matches
    assert(memcmp(PSX_get_metadata(&file), &met, PSX_get_metadata_size(&file)) == 0);
    // Read from file
    int64_t *rsamples = malloc(PSX_get_buf_size(&file, 1));

    for (int frame=0; frame<NB_FRAMES; frame++) {
        PSX_read_frames(&file, rsamples, frame, 1);
        for (int chan=0; chan<NB_CHANNELS; chan++) {
            // Check if we find the data above
            int64_t expected_frame = rsamples[0];
            int64_t expected_chan = rsamples[chan*CHANNEL_SIZE+1];
            int64_t expected_term = rsamples[chan*CHANNEL_SIZE+CHANNEL_SIZE-1];
            assert(expected_frame == frame);
            assert(expected_chan == chan);
            assert(expected_term == 0xff);
            for (int i=2; i<CHANNEL_SIZE-1; ++i) {
                if(rsamples[chan*CHANNEL_SIZE+i] != i + frame + chan) {
                    printf("data wrong at sample %i, frame %i channel %i\n", i, frame, chan);
                    printf("Expected %i got %lu\n", i + frame + chan, rsamples[i]);
                    assert(0);
                }
            }
        }
    }
    PSX_close(&file);
    printf("[INFO] File closed\n");
    free(rsamples);
    return err;
}