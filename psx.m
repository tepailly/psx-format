% Read nb_frames of interleaved complex samples starting from frame_index from a PSX file
function D=psx(name, frame_index, nb_frames)
    % PSX  Read from a psx file.
    %   D = PSX(name, frame_index, nb_frames)
    if nb_frames < 1
        error("PSX : nb_frames should be at least 1");
    end
    % Open file in read mode, PSX file byte order is little endian
    [fid, msg] = fopen(name, "r", "l");
    if fid < 0 
        error(msg);
    end
    % Check PSX magic number
    magic = fread(fid, 4, "uint8=>char");
    expected_magic = ['.';'P';'S';'X'];
    if magic ~= expected_magic
        error('PSX : %s is not a PSX file', name);
    end
    % Read usefull metadata
    fseek(fid, 7, "bof");
    type_size = fread(fid, 1, "uint8");
    % We would need to fseek to byte 8 but we already are at byte 8 from 
    % the previous read
    file_nb_frames = fread(fid, 1, "uint32");
    if frame_index + nb_frames > file_nb_frames
        error('PSX : Cannot read %d frames from a file that has %d frames, starting at frame %d', ...
            nb_frames, file_nb_frames, frame_index);
    end
    fseek(fid, 16, "bof"); % skip CRC
    channel_size = fread(fid, 1, "uint32");
    metadata_size = fread(fid, 1, "uint16");
    nb_channels = fread(fid, 1, "uint8");
    % Move to the correct location in the file
    start = 24 + metadata_size; % PSX header is 24 bytes, skip the metadata block
    offset = start + type_size * channel_size * nb_channels * frame_index; % move to the desired frame
    fseek(fid, offset, "bof");
    % Read nb_frames frames from the file
    D = fread(fid, nb_frames*nb_channels*2*channel_size, "int32");
    fclose(fid);
    % Convert interleaved complex to matlab complex
    D = reshape(D, [2, nb_frames * nb_channels * channel_size]);
    D = D(1,:) + 1j*D(2,:);
    % Reshape to matlab way of adressing 3D arrays
    D = reshape(D, [channel_size, nb_channels, nb_frames]);
    % Permute dimensions in the human order (nb_frames, nb_channels,
    % channel_size)
    D = permute(D, [3 2 1]);
end