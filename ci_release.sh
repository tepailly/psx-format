#!/bin/bash
# Compile script used by the holy build box continuous integration, will not work on your machine

# Exit when a command returns a non zero status
set -e

# Activate Holy Build Box environment.
source /hbb_exe/activate

# Print commands before they are executed
set -x

# Compile the tool and the libraries
make tool_release
make libs