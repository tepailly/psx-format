/**
 * @file psx.h
 * @author Thomas Epailly (thomas.epailly@alumni.enac.fr)
 * @brief PARSAX Binary file format, definitions and read/write functions.
 * @details This file format is meant to be as simple and fast as possible, 
 * random writes are not supported. Writes have to be ordered, from the first to the last.
 * Random reads however are supported via the function PSX_read_frames().
 * Only the little-endian byte order is supported.
 * @version 1.2
 * @date 2024-07-04
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/**
 * @mainpage
 *
 * <h3>C API for the PSX file format</h3>
 *
 * This document contains the API docs for the PSX file format. Usage is explained below.
 *
 * <h3>Usage</h3>
 *
 * A PSX file should be opened either in PSX_MODE_READ or PSX_MODE_WRITE mode with the function PSX_open().
 * After all read or write operations have completed, the PSX file should be closed with PSX_close().<br>
 * Unlike the Python or MatLab implementations, the C implementation is type-agnostic. You can store any samples
 * with a size up to 255 bytes per sample.
 *
 * <h4>Write mode</h4>
 * In write mode, before writing to the file you have to call the functions that will configure the file
 * for proper writing. Have a look at @ref write_funcs.<br>
 * 
 * 
 * <h4>Read mode</h4>
 * In read mode, all of the file parameters are determined when it is opened.<br>
 * To get information for the file, have a look at @ref read_funcs.
 *
 **/
#ifndef PSX_FILE_FORMAT
#define PSX_FILE_FORMAT

#define PSX_VERSION_MAJOR 1
#define PSX_VERSION_MINOR 3

/** Number to 8 byte chunks to read and process at a time when computing the file's CRC32. */
#define PSX_CRC32_8CHUNK_SIZE 65536 // Which is 500kiB per chunk

#ifndef _LARGEFILE_SOURCE 
#define _LARGEFILE_SOURCE // obsolete, but used by HBB CI
#endif // _LARGEFILE_SOURCE
#include <stdio.h>

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <nmmintrin.h> // CRC32 Intel Intrinsics

#ifdef __cplusplus
extern "C" {
#endif

// ----- PSX file enums -----
/**
 * @brief PSX File byte orders
 * @details This is only used as a guard to alert the user when there is a byte order mismatch.
 * 
 */
enum PSX_Byte_Order {
    PSX_ORDER_OTHER, ///< Other byte order
    PSX_ORDER_LITTLE_ENDIAN, ///< Little endian order
    PSX_ORDER_BIG_ENDIAN, ///< Big endian byte order
};

/**
 * @brief Store the system's byte order in the PSX_BYTE_ORDER macro.
 * This is not an optimal way of determining the byte order of a system but
 * gives an idea.
 */
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define PSX_BYTE_ORDER PSX_ORDER_LITTLE_ENDIAN
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define PSX_BYTE_ORDER PSX_ORDER_BIG_ENDIAN
#endif

#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#warning "[IMPORTANT] The PSX file format does not support conversion between byte orders yet! Make sure your system is little endian."
#endif

/**
 * @brief PSX file access modes
 * 
 */
enum PSX_File_Mode {
    PSX_MODE_READ, ///< File read mode
    PSX_MODE_WRITE ///< File write mode
};

/**
 * @brief Header of the PSX file
 * 
 */
typedef struct __attribute__ ((__packed__)) { // align this structure to 1 byte instead of 8 (on 64bit-OS)
    // PSX file description
    char magic_number[4]; // Identifies the file type
    uint8_t version_major;
    uint8_t version_minor;
    uint8_t endianness;
    uint8_t type_size; // Size of the data type
    
    uint32_t nb_frames; // How many frames do we have in this file
    uint32_t crc; // CRC of the file
    
    uint32_t channel_size; // How many samples per channel
    uint16_t metadata_size; // Size of the user metadata in bytes
    uint8_t nb_channels; // How may channels are stored per frame
    uint8_t __align; // reserved/unused
} PSXHeader;

/**
 * @brief PSX file configuration holder 
 * 
 */
typedef struct {
    int mode; // PSX_MODE_READ or PSX_MODE_WRITE
    PSXHeader head; // Stores the metadata of the PSX file
    uint8_t channel_mapping[4]; // Which index is which channel
    uint32_t current_channel; // Tracks the channel number
    uint32_t crc; // CRC of the file read
    void *metadata_ptr; // Pointer to the user metadata
    FILE *fh; // file handler
} PSXFile;

/**
 * @defgroup common_funcs PSX common interface. 
 * Common functions that can be used in read or write mode.<br>
 * 
 * @{
 */
int PSX_open(PSXFile *conf, char *, int);
uint32_t PSX_compute_CRC(PSXFile*);
int PSX_close(PSXFile *c);
/** @} */ // end of common_funcs

/**
 * @defgroup write_funcs Writing to a PSX file
 * Functions used to write to a PSX file opened in PSX_MODE_WRITE.
 * <h4>Writing to a file</h4>
 * In order for you to write to a PSX file opened in PSX_MODE_WRITE, you will first need to setup
 * the file format by calling the PSX_set_* familly of functions.<br>
 * You <b>have to</b> call at least PSX_set_type_size(), PSX_set_channel_size(), PSX_set_nb_channels() 
 * before writting to the file.<br>
 * In the case you want to add metadata, call PSX_set_metadata_size() with a non null size.<br>
 * Then, to write to the file, you will need to write channel by channel sequencialy using PSX_write_channel(). 
 * When you have wrote nb_channels, the nb_frames counter will increment by one.<br>
 * In the case you have set a non null metadata size, add the metadata to the file by using 
 * PSX_set_metadata() before calling PSX_close().<br>
 * In the case you want to add the CRC32 checksum, you should also call PSX_add_CRC() before closing the file.
 * When PSX_close() is called, file header and user metadata (if any) is written at the begining of the file.
 * @{
 */
void PSX_set_metadata_size(PSXFile*, uint16_t);
void PSX_set_type_size(PSXFile *, uint8_t);
void PSX_set_nb_channels(PSXFile *, uint8_t);
void PSX_set_channel_size(PSXFile *, uint32_t);
void PSX_set_metadata(PSXFile *, void*);
int PSX_write_channel(PSXFile *c, void *);
void PSX_add_CRC(PSXFile*);
/** @} */ // end of write_funcs

/**
 * @defgroup read_funcs Reading from a PSX file
 * Functions used to read data and metadata from the PSX file opened in PSX_MODE_READ.
 * <h4>General usage</h4>
 * You can gather information from the file by using the PSX_get_* familly of functions
 * that will read metadata from the PSX header and user metadata.<br>
 * Then, before reading the file data, you need to allocate sufficient space to hold
 * the number of frames you want to read. You can do so with PSX_get_buf_size(). <br>
 * You can then read at most nb_frames from the file into your buffer by using
 * the PSX_read_frames() function.
 * <h4>Important note about user metadata</h4>
 * User metadata can be read from the file with the functions PSX_get_metadata_size()
 * and PSX_get_metadata(). In the case there is no metadata, the functions will return
 * respectively 0 and NULL.<br>
 * In the case there is user metadata in the file, the pointer returned by PSX_get_metadata()
 * will remain valid until PSX_close() is called, after which the pointer is freed.
 * @{
 */
uint32_t PSX_get_CRC(PSXFile*);
int PSX_check_CRC(PSXFile *conf);
uint16_t PSX_get_metadata_size(PSXFile*);
uint8_t PSX_get_type_size(PSXFile*);
uint8_t PSX_get_nb_channels(PSXFile*);
uint32_t PSX_get_channel_size(PSXFile*);
void* PSX_get_metadata(PSXFile*);
uint32_t PSX_get_nb_frames(PSXFile*);
size_t PSX_get_buf_size(PSXFile*, uint32_t);
int PSX_read_frames(PSXFile*, void*, uint32_t, uint32_t);
/** @} */ // end of read_funcs

#ifdef __cplusplus
}
#endif

#endif // PSX_FILE_FORMAT