/**
 * @file tool.c
 * @author Thomas Epailly (thomas.epailly@alumni.enac.fr)
 * @brief Tool to read metadata of a PSX file and to check it's CRC
 * @version 0.1
 * @date 2024-08-30
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "psx.h"

int main(int argc, char *argv[]) {
    int err = 0;
    printf("---- PSXTool ----\n");
    if (argc != 2) {
        printf("Usage : psxtool path/to/psx/file\n");
    }
    else {
        char *path = argv[1];
        int err;
        PSXFile *f = malloc(sizeof(PSXFile));
        
        if(PSX_open(f, path, PSX_MODE_READ) > 0) {
            err = 1;
            goto end;
        }

        printf("Metadata of PSX file \"%s\"\n", path);
        printf(
            " - PSX file version %u.%u\n"
            " - Number of frames : %u\n"
            " - Number of channels per frame : %u\n"
            " - Number of samples per channel : %u\n"
            " - Size of a single sample : %u bytes\n"
            " - Size of the user metadata : %u bytes\n",
            f->head.version_major, f->head.version_minor,
            PSX_get_nb_frames(f),
            PSX_get_nb_channels(f),
            PSX_get_channel_size(f),
            PSX_get_type_size(f),
            PSX_get_metadata_size(f)
        );

        // Compute the CRC
        uint32_t crc = PSX_compute_CRC(f);

        if(crc == f->head.crc)
            printf(" - CRC match : 0x%08X\n", PSX_get_CRC(f));
        else {
            printf("CRC mismatch : file has CRC=0x%08X, computed was CRC=0x%08X\n",
            PSX_get_CRC(f), crc);
            err = 1;
        }

        PSX_close(f);
end:
        free(f);
    }
    return err;
}