/**
 * @file parsax_metadata.h
 * @author Thomas Epailly (thomas.epailly@alumni.enac.fr)
 * @brief PARSAX metadata example for PSX file format
 * @version 1.0
 * @date 2024-07-17
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef PARSAX_METADATA_H
#define PARSAX_METADATA_H

#include <stdint.h>

#define PARSAX_METADATA_VERSION_MAJOR 1
#define PARSAX_METADATA_VERSION_MINOR 1

// ----- PARSAX metadata enums -----
/**
 * @brief The window type used at acquisition.
 * 
 */
enum PARSAX_Window {
    PARSAX_WIN_UNSPECIFIED, ///< Unspecified window (default).
    PARSAX_WIN_RECT, ///< Rectangular window
    PARSAX_WIN_HANN, ///< Hann window
    PARSAX_WIN_HAMMING, ///< Hamming window
    PARSAX_WIN_BLACKMAN ///< Blackman window
};

/**
 * @brief The domain in which the data is in.
 * 
 */
enum PARSAX_Data_Domain {
    PARSAX_DOMAIN_UNSPECIFIED, ///< Unspecified domain (default).
    PARSAX_DOMAIN_FREQUENCY, ///< FFT was performed on data 
    PARSAX_DOMAIN_TEMPORAL ///< FFT was skipped during acquisition
};

/**
 * @brief Specifies what data is stored in a given channel.
 * 
 */
enum PARSAX_Channels {
    PARSAX_CHAN_EMPTY, ///< No data in this channel
    PARSAX_CHAN_UNSPECIFIED, ///< Unspecified data (default).
    PARSAX_CHAN_HH, ///< H transmitted mixed with H received
    PARSAX_CHAN_VV, ///< V transmitted mixed with V received
    PARSAX_CHAN_VH, ///< V transmitted mixed with H received
    PARSAX_CHAN_HV ///< H transmitted mixed with V received
};

// Default initializer for parsax metadata
#define PARSAX_METADATA_INIT {\
    .version_major = PARSAX_METADATA_VERSION_MAJOR,\
    .version_minor = PARSAX_METADATA_VERSION_MINOR,\
    .__align = {0,0},\
    .channel_mapping = {PARSAX_CHAN_UNSPECIFIED, PARSAX_CHAN_UNSPECIFIED, PARSAX_CHAN_UNSPECIFIED, PARSAX_CHAN_UNSPECIFIED},\
    .window = PARSAX_WIN_UNSPECIFIED,\
    .domain = PARSAX_DOMAIN_UNSPECIFIED,\
    .mixer_disabled = 0,\
    .gain_index = 0,\
    .sample_delay = 0,\
    .window_offset = 0,\
}

// ----- PARSAX metadata structure -----

typedef struct {
    uint8_t version_major;
    uint8_t version_minor;
    uint8_t __align[2];
    uint8_t channel_mapping[4];

    uint8_t window;
    uint8_t domain;
    uint8_t mixer_disabled;
    uint8_t gain_index;
    uint16_t sample_delay;
    uint16_t window_offset;

    uint64_t timestamp_s;
} PARSAX_Metadata;

#endif