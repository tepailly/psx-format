"""
Read 32 bit interleaved complex data from a PSX file.

PSX version 1.3
Thomas Epailly
"""

import numpy as np
import numpy.typing as npt
import struct
import typing

PSX_VERSION_MAJOR = 1
PSX_VERSION_MINOR = 3

PSX_HEADER_SIZE     = 24
PSX_HEADER_FORMAT   = "<ccccBBBB"   # bytes 0-7
PSX_HEADER_FORMAT  += "II"       # bytes 8-15
PSX_HEADER_FORMAT  += "IHBx"        # bytes 16-23

class PSXFile:
    def __init__(self, fp:str):
        # Open the file in read binary mode
        self.fh = open(fp, "rb")
        header = self.fh.read(PSX_HEADER_SIZE)
        (
            m0,
            m1,
            m2,
            m3,
            version_major,
            version_minor,
            byte_order,
            type_size,
            nb_frames,
            crc,
            channel_size,
            metadata_size,
            nb_channels
        ) = struct.unpack(PSX_HEADER_FORMAT, header)
        
        magic = ''.join([c.decode() for c in (m0, m1, m2, m3)])
        if magic != ".PSX":
            raise Exception(f"{fp} is not a PSX file")
        if  version_major != PSX_VERSION_MAJOR or version_minor != PSX_VERSION_MINOR:
            raise Exception(f"Cannot read PSX file {fp}, file is v{version_major}.{version_minor}")
        if byte_order != 1:
            raise Exception(f"PSX file {fp} has invalid byte order")

        self.type_size = type_size
        self.nb_frames = nb_frames
        self.nb_channels = nb_channels
        self.channel_size = channel_size
        self.metadata_size = metadata_size

        self.frames_read = 0

        # Move the cursor to the begining of the frames
        start = PSX_HEADER_SIZE + self.metadata_size
        self.fh.seek(start, 0)

        # Assuming PARSAX data
        self.dtype = np.dtype([('re', '<i4'), ('im', '<i4')])

    def read_frame(self) -> np.ndarray[typing.Any, np.complex128]:
        """
        Reads a frame from the PSX file
        """
        if self.frames_read + 1 < self.nb_frames:
            # Read samples
            res = np.fromfile(self.fh, dtype=self.dtype, count=self.nb_channels * self.channel_size)
            # Convert to complex floats
            res = res.view(np.int32).astype(np.float64).view(np.complex128)
            self.frames_read += 1
            # Reshape the output
            return res.reshape((self.nb_channels, self.channel_size))
        else:
            return np.array([], dtype=self.dtype)

    def read_frames_at(self, nb_frames, at=0) -> np.ndarray[typing.Any, np.complex128]:
        """
        Reads nb_frames frames starting at frame 'at'
        """
        # Check input
        if at + nb_frames >= self.nb_frames:
            raise Exception(f"PSX : Cannot read {nb_frames} frames starting at frame #{at} in a file that contains {self.nb_frames} frames.")
        # Compute offset
        start = PSX_HEADER_SIZE + self.metadata_size
        offset = start + self.type_size * self.channel_size * self.nb_channels * at
        self.fh.seek(offset, 0)
        # Read file
        res = np.fromfile(self.fh, dtype=self.dtype, count=nb_frames * self.nb_channels * self.channel_size)
        res = res.view(np.int32).astype(np.float64).view(np.complex128)
        return res.reshape((nb_frames, self.nb_channels, self.channel_size))

    def read_frames(self, nb_frames) -> np.ndarray[typing.Any, np.complex128]:
        """
        Read nb_frames frames from the PSX file
        """
        if self.frames_read + nb_frames < self.nb_frames:
            res = np.fromfile(self.fh, dtype=self.dtype, count=nb_frames * self.nb_channels * self.channel_size)
            res = res.view(np.int32).astype(np.float64).view(np.complex128)
            self.frames_read += nb_frames
            return res.reshape((nb_frames, self.nb_channels, self.channel_size))
        else:
            return np.array([], dtype=self.dtype)

    def close(self):
        self.fh.close()

# Test code
if __name__ == "__main__":
    f = PSXFile("test.psx") # Reads the generated test.psx file from the C program
    f.read_frame()
    print(f.read_frame())
    f.read_frames(2)
    f.read_frames_at(2, 1)
    f.close()