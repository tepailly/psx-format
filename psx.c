/**
 * @file psx.c
 * @author Thomas Epailly (thomas.epailly@alumni.enac.fr)
 * @brief 
 * @version 1.2
 * @date 2024-07-17
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include "psx.h"
#include <stdint.h>
#include <stdio.h>
/**
 * @brief Open a PSX file in read or write mode.
 * @details When opened in PSX_MODE_READ, the file metadata will be copied in c.
 * In PSX_MODE_WRITE mode, file will be opened for write. The user metadata should be set
 * before calling PSX_close().
 * 
 * @param file_path 
 * @param c 
 * @param mode 
 * @return int non null on error, errno on file open errors
 */
int PSX_open(PSXFile *c, char *file_path, int mode) {
    // Initialize values to safe defaults
    // Default PSX file metadata (overwritten in read mode)
    memcpy(&c->head.magic_number, ".PSX", 4);
    c->head.version_major = PSX_VERSION_MAJOR;
    c->head.version_minor = PSX_VERSION_MINOR;
    c->head.endianness = PSX_BYTE_ORDER;
    c->head.type_size = 0;
    c->head.nb_frames = 0;
    c->head.nb_channels = 0;
    c->head.channel_size = 0;
    c->head.metadata_size = 0;
    c->head.crc = 0;

    // Default file configuration
    c->current_channel = 0;
    c->fh = NULL;
    c->mode = mode;
    c->crc = 0;
    c->metadata_ptr = NULL;
    
    // Open output file
    if(mode == PSX_MODE_WRITE) {
        // Open for reading and writing.  The file is created if it does not exist, otherwise it is truncated.  
        // The stream is positioned at the beginning of the file.
        c->fh = fopen(file_path, "w+"); // w+ because we read from the file when computing the CRC
        if(c->fh == NULL)
            fprintf(stderr, "[ERROR] PSX could not open file %s for write : %s\n", file_path, strerror(errno));
    }
    else if(mode == PSX_MODE_READ) {
        c->fh = fopen(file_path, "r");
        if(c->fh == NULL)
            fprintf(stderr, "[ERROR] PSX could not open file %s for reading : %s\n", file_path, strerror(errno));
    }
    if(c->fh == NULL)
        return errno;
    else {
        if(mode == PSX_MODE_READ) {
            // Read the header
            int read = fread(&c->head, sizeof(PSXHeader), 1, c->fh);
            if(read < 1) {
                fprintf(stderr, "[ERROR] Could not read header from \'%s\'\n", file_path);
                fclose(c->fh);
                return 1;
            }
            // If the file byte order does not match the system's byte order, print a warning
            if(c->head.endianness != PSX_BYTE_ORDER)
                fprintf(stderr, "[WARNING] PSX : Potential byte order mismatch between the file and the system.\n");
            if(c->head.version_major != PSX_VERSION_MAJOR || c->head.version_minor != PSX_VERSION_MINOR)
                fprintf(stderr, "[WARNING] PSX : Read file has version V%u.%u, reading program is using V%u.%u\n",
                    c->head.version_major,
                    c->head.version_minor,
                    PSX_VERSION_MAJOR,
                    PSX_VERSION_MINOR
                );
            // Store it's data into configuration struct
            // PSX file description
            PSX_set_metadata_size(c, c->head.metadata_size); // Set metadata size and allocate memory to cache it if needed
            c->crc = c->head.crc;
            // Read metadata into receiving buffer if needed
            if(c->head.metadata_size > 0) {
                read = fread(c->metadata_ptr, c->head.metadata_size, 1, c->fh);
                if(read < 1) {
                    fprintf(stderr, "[ERROR] PSX : Could not read metadata from \'%s\'\n", file_path);
                }
            }
        }
    }
    return 0;
}

/**
 * @brief Set the size of the user metadata, allocate cache for it.
 * 
 * @param metadata_size Size of the user metadata in bytes.
 */
void PSX_set_metadata_size(PSXFile *c, uint16_t metadata_size) {
    c->head.metadata_size = metadata_size;
    if(metadata_size > 0)
        c->metadata_ptr = calloc(1, metadata_size);
}

/**
 * @brief Sets the file-wide type size or sample size.
 * 
 * @param c 
 * @param n sample size
 */
void PSX_set_type_size(PSXFile *c, uint8_t n) {
    c->head.type_size = n;
}
/**
 * @brief Set the number of samples per channel.
 * 
 * @param c 
 * @param n channel size
 */
void PSX_set_channel_size(PSXFile *c, uint32_t n) {
    c->head.channel_size = n;
}
/**
 * @brief Set how many channels are stored in a single frame.
 * 
 * @param c 
 * @param n number of channels
 */
void PSX_set_nb_channels(PSXFile *c, uint8_t n) {
    c->head.nb_channels = n;
}

/**
 * @brief Sets the metadata to put in the file, only used in write mode.
 * 
 * @param c 
 * @param met pointer to the metadata structure
 */
void PSX_set_metadata(PSXFile *c, void *met) {
    if(c->mode == PSX_MODE_READ) {
        fprintf(stderr, "[WARNING] PSX : Cannot set metadata in read mode\n");
        return;
    }
    else if(c->mode == PSX_MODE_WRITE) {
        // Copy the provided metadata into our pointer
        memcpy(c->metadata_ptr, met, c->head.metadata_size);
    }
}

/**
 * @brief Write the contents of a single channel to the file.
 * @details This function writes channels to the file sequencially (from the first channel to the last channel).
 * The init function had to be called with the PSX_MODE_WRITE mode.
 * @param c 
 * @param samples A buffer of at least frame_size * type_size
 * @return int non-null on error
 */
int PSX_write_channel(PSXFile *c, void *samples) {
    if(c->mode == PSX_MODE_READ)
        return 1; // invalid mode

    // If no data has been written yet, move the cursor to the begining of the data
    if(c->head.nb_frames == 0 && c->current_channel == 0)
        fseeko(c->fh, sizeof(PSXHeader) + c->head.metadata_size, SEEK_SET);

    // Number of samples of size c->type_size already written to the file
    size_t samples_written = 0;
    while (samples_written < c->head.channel_size) {
        // Write the content of the channel to the output file
        // A void* pointer is a 8 byte address (on 64 bit systems) pointing to a single byte (sizeof(void*) == 8 && sizeof(void) == 1)
        // At each iteration we offset the samples pointer by the amount samples already written
        samples_written += fwrite((uint8_t*)samples + (c->head.type_size * samples_written), c->head.type_size, c->head.channel_size - samples_written, c->fh);
        // Note : samples + (c->type_size * samples_written) is the same as &samples[c->type_size * samples_written]
        // But in the latter we are dereferencing a void* pointer which is a war crime
    }
    c->current_channel++;
    // Keep track of the channel and frame counters
    if(c->current_channel == c->head.nb_channels) {
        c->head.nb_frames++; // Increment the number of frames
        c->current_channel=0; // Go back to channel 0
    }
    return 0;
}
/**
 * @brief Adds the CRC to a file opened in write mode.
 * @details This function has to be called before PSX_close.
 * 
 * @param c 
 */
void PSX_add_CRC(PSXFile *c) {
    c->head.crc = PSX_compute_CRC(c);
}

/**
 * @brief Get the CRC stored in the currently opened PSX file.
 * @details This function will only display the CRC of the file being read.
 * In the case you want to check/compute the CRC, have a look at PSX_check_CRC() or PSX_compute_CRC().
 * 
 * @param c 
 * @return uint32_t 
 */
uint32_t PSX_get_CRC(PSXFile *c) {
    return c->head.crc;
}
/**
 * @brief Checks the CRC of a file opened in read mode.
 * @details This function will compute the expected CRC with PSX_compute_CRC() and compare it 
 * to the CRC stored in the file being read.
 * 
 * @param c 
 * @return int 0 on CRC match, 1 on mismatch
 */
int PSX_check_CRC(PSXFile *c) {
    if (c->mode == PSX_MODE_READ) {
        // Compute CRC
        uint32_t computed_crc = PSX_compute_CRC(c);
        //printf("computed : %08X, read : %08X\n", computed_crc, c->head.crc);
        return computed_crc != c->head.crc;
    }
    else
        printf("[WARNING] PSX : Cannot check the file CRC in write mode.\n");
    return 1;
}
/**
 * @brief Get the sample size.
 * 
 * @param c 
 * @return uint8_t size in bytes of a single sample
 */
uint8_t PSX_get_type_size(PSXFile *c) {
    return c->head.type_size;
}
/**
 * @brief Get the channel size.
 * 
 * @param c 
 * @return uint32_t number of samples per channel 
 */
uint32_t PSX_get_channel_size(PSXFile *c) {
    return c->head.channel_size;
}
/**
 * @brief Get the number of channels.
 * 
 * @param c 
 * @return uint8_t Number of channels per frame
 */
uint8_t PSX_get_nb_channels(PSXFile *c) {
    return c->head.nb_channels;
}
/**
 * @brief Get the number of frames.
 * 
 * @param c 
 * @return uint32_t Number of frames stored in the PSX file.
 */
uint32_t PSX_get_nb_frames(PSXFile *c) {
    return c->head.nb_frames;
}
/**
 * @brief Get the size of the user metadata.
 * 
 * @param c 
 * @return uint16_t Size of the metadata in bytes, can be 0 if there is no user metadata.
 */
uint16_t PSX_get_metadata_size(PSXFile *c) {
    return c->head.metadata_size;
}
/**
 * @brief Get the user metadata.
 * @details The pointer remains valid until PSX_close() is called.
 * 
 * @param c 
 * @return void* Pointer to the user metadata, can be NULL if there is no user metadata.
 */
void* PSX_get_metadata(PSXFile *c) {
    return c->metadata_ptr;
}
/**
 * @brief Helper to compute the buffer size required to hold nb_frames.
 * 
 * @param c 
 * @param nb_frames Number of frames to hold
 * @return size_t minimal size of the buffer in bytes
 */
size_t PSX_get_buf_size(PSXFile *c, uint32_t nb_frames) {
    //                 depends                     channel_size                nb_channels              nb_frames
    return (size_t)c->head.type_size * (size_t)c->head.channel_size * (size_t)c->head.nb_channels * (size_t)nb_frames;
}
/**
 * @brief Read nb_frames into buff.
 * @details This function will read all channels from each frame.
 * 
 * @param c 
 * @param buff Receving buffer, has to be at least PSX_get_buf_size(c, nb_frames)
 * @param frame_start_index Starting frame index
 * @param nb_frames Number of frames to read
 * @return int non-null on error
 */
int PSX_read_frames(PSXFile *c, void *buff, uint32_t frame_start_index, uint32_t nb_frames) {
    if(c->mode == PSX_MODE_WRITE)
        return 1;

    // Move to the correct location in the file
    // Convert to off_t to avoid integer overflows
    off_t bytes_offset =  (off_t)c->head.type_size * (off_t)c->head.channel_size * (off_t)c->head.nb_channels; // Size of a frame
    bytes_offset *= (off_t)frame_start_index; // Move by frame_start_index frames
    bytes_offset += (off_t)sizeof(PSXHeader) + (off_t)c->head.metadata_size; // Skip file headers
    
    //printf("frame %u, bytes offset : %li\n", frame_start_index, bytes_offset);

    fseeko(c->fh, bytes_offset, SEEK_SET);

    // Number of int32_t to read from the file
    size_t samples_to_read = PSX_get_buf_size(c, nb_frames) / c->head.type_size;
    // Number of samples read
    size_t total_samples_read = 0; // total
    size_t samples_read = 0; // local
    while(total_samples_read < samples_to_read) {
        samples_read = fread(buff, c->head.type_size, samples_to_read-samples_read, c->fh);
        if (samples_read == 0) {
            fprintf(stderr, "[ERROR] PSX read : fread returned 0, file metadata might be invalid\n");
            return 1;
        }
        total_samples_read += samples_read;
    }
    return 0;
}

/**
 * @brief Compute the CRC of the currently opened PSX file.
 * @details In PSX_MODE_WRITE, the call to this function is valid after PSX_set_* functions have been called
 * and after all frames have been written. <br>
 * In PSX_MODE_READ, this function can be called after a successful call to PSX_open().<br>
 * This function uses the CRC32 polynomial 0x11EDC6F41/0x1EDC6F41 and 0xFFFFFFFF initial value 
 * to compute the CRC. Intel intrisics SSE4.2 instructions _mm_crc32_* are used for fast processing.<br>
 * The CRC is known/used as/in CRC-32/ISCSI, CRC-32/BASE91-C, CRC-32/CASTAGNOLI, CRC-32/INTERLAKEN, CRC-32C <br>
 * More info can be found here https://crccalc.com/?crc=Hello&method=CRC-32/ISCSI&datatype=0&outtype=0<br>
 *
 * The CRC32 of the ASCII string "123456789" should give 0xE3069283.
 * 
 * @param c 
 * @param h Pointer to PSX file header
 * @return uint32_t 
 */
uint32_t PSX_compute_CRC(PSXFile *c) {
    // A 64 bit integer is used because _mm_crc32_u64 returns a 64 bit integer that represents the 32 bit CRC
    uint64_t crc = 0xFFFFFFFF; // CRC initial value
    
    // Cache current header
    PSXHeader *head = (PSXHeader*) malloc(sizeof(PSXHeader));
    memcpy(head, &c->head, sizeof(PSXHeader));

    head->crc = 0; // Set CRC to zero for computation

    // Process the PSX file header byte per byte
    // Cast header into a uint32_t[6]
    uint8_t *data = (uint8_t *)head;
    for (size_t i=0; i<sizeof(PSXHeader); ++i) {
        // Intel intrinsics, fast CRC32
        crc = (uint64_t) _mm_crc32_u8(crc, data[i]);
        //printf("%02X", data[i]);   
    }

    // Process the metadata if any, byte per byte
    if(c->head.metadata_size > 0 && c->metadata_ptr != NULL) {
        uint8_t *data = (uint8_t *)c->metadata_ptr;
        for (size_t i=0; i<c->head.metadata_size; ++i) {
            // Intel intrinsics, fast CRC32
            crc = (uint64_t) _mm_crc32_u8(crc, data[i]);
            //printf("%02X", data[i]);
        }
    }

    // Process the file data, by reading chunks of nb_ints*8 and computing the CRC 8 bytes per clock cycle
    // Reposition the cursor at the begining of the file data
    fseeko(c->fh, sizeof(PSXHeader) + c->head.metadata_size, SEEK_SET);
    // Convert to size_t to avoid integer overflows
    size_t data_size = (size_t)head->type_size * (size_t)head->channel_size * (size_t)head->nb_channels * (size_t)head->nb_frames;

    uint64_t *buffer = (uint64_t*) calloc(PSX_CRC32_8CHUNK_SIZE, sizeof(uint64_t));
    size_t ints_read = 0;
    unsigned long read;
    while(ints_read < data_size/8) {
        // "read" contains the number of uint64_t read from the file
        read = fread(buffer, sizeof(uint64_t), PSX_CRC32_8CHUNK_SIZE, c->fh);
        ints_read += read;
        if(read > 0) {
            for (size_t i=0; i<read; ++i)
                // _mm_crc32_u64 outputs a 64bit integer but still represents the 32 bit CRC
                crc = _mm_crc32_u64(crc, buffer[i]); // latency : 3, thoughput : 1
                // This can be optimized further by running 3 _mm_crc32_u64 independant
                // instructions and recombining them later. Doing so would triple the bandwidth.
        }
        else
            break; // Less than 8 bytes left or EOF reached
    }
    free(buffer);

    uint8_t buff[8];
    // Read the remaining bytes, if any
    read = fread(buff, sizeof(uint8_t), 8, c->fh);
    for (size_t i=0; i<read; ++i) // process individual bytes
        crc = (uint64_t) _mm_crc32_u8(crc, buff[i]); // 1 CPU clock cycle

    const size_t bytes_read = ints_read*8 + read;
    if(data_size != bytes_read)
        fprintf(stderr, "[ERROR] PSX : CRC data size was %lu bytes and read %lu bytes\n", data_size, bytes_read);

    free(head);
    // Convert the 64 bit integer representing the 32bit CRC to a 32 bit integer
    uint32_t crc32 = (uint32_t) crc;
    // Finalize the CRC with a XOR
    crc32 = crc32 ^ 0xFFFFFFFF;
    return crc32;
}

/**
 * @brief Close the file.
 * @details In PSX_MODE_WRITE, file metadata is written.<br>
 * The pointer returned by PSX_get_metadata() is freed and no longer valid.
 * 
 * @param c 
 * @return int non null on error
 */
int PSX_close(PSXFile *c) {
    if(c->mode == PSX_MODE_WRITE) {
        // Write the header
        fseeko(c->fh, 0, SEEK_SET); // go to the begining of the file
        size_t items_written;
        items_written = fwrite(&c->head, sizeof(PSXHeader), 1, c->fh); // write header contents
        if(items_written < 1)
            fprintf(stderr, "[ERROR] PSX : Could not write header to output file\n");
        // Write the metadata if needed
        if(c->head.metadata_size > 0) {
            items_written = fwrite(c->metadata_ptr, c->head.metadata_size, 1, c->fh);
            if(items_written < 1)
                fprintf(stderr, "[ERROR] PSX : Could not write metadata to output file\n");
        }
    }
    // Close the file
    int err = 0;
    if(c->fh != NULL)
        err = fclose(c->fh);
    // Free memory
    if(c->head.metadata_size > 0)
        free(c->metadata_ptr);
    return err;
}